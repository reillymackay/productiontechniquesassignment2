﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System;
using System.Reflection;
using UnityEditor;
using System.Xml;


public class LevelManager : MonoBehaviour {

    // transform variables that gameobjects in the scene are plugged in to.  Within each transform object is each of the tiles in that layer of the .tmx file.
    public Transform groundHolder;
    public Transform turretHolder;
    public Transform addedElementHolder;

    // From what I understand, these are ways to encode an image/non-text file to text to be read by unity.
    public enum MapDataFormat {
        Base64,
        CSV
    }

    // makes the enum list a dropdown menu in the inspector.
    public MapDataFormat mapDataFormat;

    // grabs the sprite sheet that the .tmx file reads.
    public Texture2D spriteSheetTexture;

    // gameobjects are prefabs that get spawned into the Transforms in the scene, referenced at the top of the script. Tiles into ground, turrets into turrets.
    public GameObject tilePrefab;
    public GameObject turretPrefab;
    public GameObject addedElementPrefab;

    // Mapsprites assigns each tile in the map spritesheet a spot on the sprite list (goes up to 48 as there's 48 unique sprites on the sheet.)
    // Tiles clone and spawn a tile in each section of the map and assign them a sprite from the mapsprites tiles given their positions in the .tmx file.
    // Turretpositions are the positions of the turrets as dictated by the tmx file.
    public List<Sprite> mapSprites;
    public List<GameObject> tiles;
    public List<Vector3> turretPositions;
    public List<Vector3> addedElementPositions;

    Vector3 tileCenterOffset;
    Vector3 mapCenterOffset;

    // the .tmx file the map is being read from, to get the positions of each tile. Assigned in inspector.
    public string TMXFilename;

    
    string gameDirectory;
    string dataDirectory;
    string mapsDirectory;
    string spriteSheetFile;
    string TMXFile;

    // Dictates tile system size. Since all tiles are 32x32, this just equates to 100% scaling of each tile.
    public int pixelsPerUnit = 32;

    // Set to null values in the inspector, as all of these are read from the desert-map.tmx file
    public int tileWidthInPixels;
    public int tileHeightInPixels;
    float tileWidth;
    float tileHeight;

    public int spriteSheetColumns;
    public int spriteSheetRows;

    public int mapColumns;
    public int mapRows;

    public string mapDataString;
    public List<int> mapData;



    // Clears the console 
    // from http://answers.unity3d.com/questions/10580/editor-script-how-to-clear-the-console-output-wind.html
    static void ClearEditorConsole() {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(new object(), null);
    }


    // Gets the children attached to this gameobject and destroys them.
    static void DestroyChildren(Transform parent) {
        for (int i = parent.childCount - 1; i >= 0; i--) {
            DestroyImmediate(parent.GetChild(i).gameObject);
        }
    }



    // NOTE: CURRENTLY ONLY WORKS WITH A SINGLE TILED TILESET
    // gr8


    /// <summary>
    ///  LoadLevel Overview:
    ///  
    /// </summary>
    public void LoadLevel() {

        // Clears console on execution of this method, and destroys the Ground and Turret children attached to the levelmanager gameobject to be replaced by the .tmx file values
        ClearEditorConsole();
        DestroyChildren(groundHolder);
        DestroyChildren(turretHolder);

        // Grabs the desert-map.tmx file by getting the filepath of the maps folder, then adding the TMX tilename to the end of it to load it
        {
            mapsDirectory = Path.Combine(Application.streamingAssetsPath, "Maps");
            TMXFile = Path.Combine(mapsDirectory, TMXFilename);
        }


        // 
        {
            // clears mapdata to assign it the values from the .tmx file
            mapData.Clear();

            string content = File.ReadAllText(TMXFile);

            // Uses the System.Xml functionality to read the TMX file's texts as XML.
            // Uses the string "content" declared above to store the .xml file's data
            using (XmlReader reader = XmlReader.Create(new StringReader(content))) {

                // ReadToFollowing allows the file to only be read to the line that first contains the string of text defined ("here").
                // All of the information needed in each segment of code is declared in each of those lines
                // The variables declared at the beginning of the script with null values are assigned values here, based on attributes applied to elements of the .tmx file
                // Because the .tmx file being opened as text reads as such: renderorder="right-down" width="20" height="15" unity can parse the values using reader.GetAttribute.
                reader.ReadToFollowing("map");
                mapColumns = Convert.ToInt32(reader.GetAttribute("width"));                         // width is 20
                mapRows = Convert.ToInt32(reader.GetAttribute("height"));                           // height is 15

                // Same functionality as explained above, gathering values from the next line of code
                reader.ReadToFollowing("tileset");
                tileWidthInPixels = Convert.ToInt32(reader.GetAttribute("tilewidth"));              //tilewidth="32"
                tileHeightInPixels = Convert.ToInt32(reader.GetAttribute("tileheight"));            //tileheight="32"

                int spriteSheetTileCount = Convert.ToInt32(reader.GetAttribute("tilecount"));       //tilecount="48"
                spriteSheetColumns = Convert.ToInt32(reader.GetAttribute("columns"));               //columns="8"
                spriteSheetRows = spriteSheetTileCount / spriteSheetColumns;                        //takes last two values to figure out how many rows there are in the tilesheet

                // Same functionality, next line
                reader.ReadToFollowing("image");
                spriteSheetFile = Path.Combine(mapsDirectory, reader.GetAttribute("source"));       //source="desert.png" - this is the plain tileset the map uses, not the map itself

                // Skips over the tile id section, as theres a segment later that reads all the tiles and cuts them up 
                reader.ReadToFollowing("layer");

                // Next line, gets encoding method
                reader.ReadToFollowing("data");
                string encodingType = reader.GetAttribute("encoding");                              //encoding="base64"

                // Picks which encoding type to use based on the file.  Because it's base64 in the .xml file it uses that case
                switch (encodingType) {
                    case "base64":
                        mapDataFormat = MapDataFormat.Base64;
                        break;
                    case "csv":
                        mapDataFormat = MapDataFormat.CSV;
                        break;
                }

                // Reads the data segment
                mapDataString = reader.ReadElementContentAsString().Trim();

                // clears the turret position to fill it with the values from the .tmx file
                turretPositions.Clear();

                // starts reading at the first objectgroup and stops reading at the next object.  The while loop makes it so that each time an object is read, it goes to the next one.
                // Goes in order top to bottom and reads each object in the objectgroup as an individual entity within the group
                // Gets the attributes just like all the other things in this method, which are the X and Y coordinates scaled to fit on the grid system declared by the PPU variable
                if (reader.ReadToFollowing("objectgroup")) {
                    if (reader.ReadToDescendant("object")) {
                        do {
                            float x = Convert.ToSingle(reader.GetAttribute("x")) / (float)pixelsPerUnit;
                            float y = Convert.ToSingle(reader.GetAttribute("y")) / (float)pixelsPerUnit;
                            turretPositions.Add(new Vector3(x, -y, 0));

                        } while (reader.ReadToNextSibling("object"));
                    }
                }

            }

            // Tells unity's XML reader how to read the files and get the needed values if the encoding format changes
            switch (mapDataFormat) {

                case MapDataFormat.Base64:

                    byte[] bytes = Convert.FromBase64String(mapDataString);
                    int index = 0;
                    while (index < bytes.Length) {
                        int tileID = BitConverter.ToInt32(bytes, index) - 1;
                        mapData.Add(tileID);
                        index += 4;
                    }
                    break;


                case MapDataFormat.CSV:

                    string[] lines = mapDataString.Split(new string[] { " " }, StringSplitOptions.None);
                    foreach (string line in lines) {
                        string[] values = line.Split(new string[] { "," }, StringSplitOptions.None);
                        foreach (string value in values) {
                            int tileID = Convert.ToInt32(value) - 1;
                            mapData.Add(tileID);
                        }
                    }
                    break;

            }

        }


        {
            // dictates the size of the tiles, given the tilesize in the .tmx file and the variable declared in this file
            // They're the same size right now, but this would scale them properly if that weren't the case
            tileWidth = (tileWidthInPixels / (float)pixelsPerUnit);
            tileHeight = (tileHeightInPixels / (float)pixelsPerUnit);

            // dictates where the centre of the map and each tile are, given their unique dimensions.
            // allows for flexibilty if tilewidth/height/mapcolumns/maprows changes
            tileCenterOffset = new Vector3(.5f * tileWidth, -.5f * tileHeight, 0);
            mapCenterOffset = new Vector3(-(mapColumns * tileWidth) * .5f, (mapRows * tileHeight) * .5f, 0);

        }




        // Gets the spritesheet the .tmx file uses and applies unity image functionality 
        {
            spriteSheetTexture = new Texture2D(2, 2);
            spriteSheetTexture.LoadImage(File.ReadAllBytes(spriteSheetFile));
            spriteSheetTexture.filterMode = FilterMode.Point;
            spriteSheetTexture.wrapMode = TextureWrapMode.Clamp;
        }


        // The segment that lays out the tiles in the spritesheet, and using the tile width and height it cuts the spritesheet up into the proper dimensions
        // Adds each one to the mapsprites list
        // Basically the tile id section but now unity can use it and apply the necessary sprite to the map
        {
            // clears the list to apply the values in the .tmx file
            mapSprites.Clear();

            for (int y = spriteSheetRows - 1; y >= 0; y--) {
                for (int x = 0; x < spriteSheetColumns; x++) {
                    Sprite newSprite = Sprite.Create(spriteSheetTexture, new Rect(x * tileWidthInPixels, y * tileHeightInPixels, tileWidthInPixels, tileHeightInPixels), new Vector2(0.5f, 0.5f), pixelsPerUnit);
                    mapSprites.Add(newSprite);
                }
            }
        }

        // Uses the previous method to apply each tile to where it needs to be
        // In the .tmx file, each tile in the spritesheet has an ID between 0 and 47
        // Therefore by adding each tile to a list in the same order, unity is able to lay them out how they are in the .tmx file
        // So now, the Tile prefabs are defined, their sprites are assigned and they're placed where they are in the .tmx file
        {
            // clears the list to apply the values in the .tmx file
            tiles.Clear();

            for (int y = 0; y < mapRows; y++) {
                for (int x = 0; x < mapColumns; x++) {

                    int mapDatatIndex = x + (y * mapColumns);
                    int tileID = mapData[mapDatatIndex];

                    GameObject tile = Instantiate(tilePrefab, new Vector3(x * tileWidth, -y * tileHeight, 0) + mapCenterOffset + tileCenterOffset, Quaternion.identity) as GameObject;
                    tile.transform.GetChild(0).GetComponent<SpriteRenderer>().sprite = mapSprites[tileID];
                    tile.transform.parent = groundHolder;
                    tiles.Add(tile);
                }
            }
        }


        // Basically the same as the last method, just for the turrets
        {
            foreach (Vector3 turretPosition in turretPositions) {
                GameObject turret = Instantiate(turretPrefab, turretPosition + mapCenterOffset, Quaternion.identity) as GameObject;
                turret.name = "Turret";
                turret.transform.parent = turretHolder;
            }
        }

        // Gets the current time and prints it in the debug console.
        DateTime localDate = DateTime.Now;
        print("Level loaded at: " + localDate.Hour + ":" + localDate.Minute + ":" + localDate.Second);
    }
}


